# UCSC PPS Assignment_06

**Recursions in C**

1) [Write a recursive function pattern(n) which prints a number triangle.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_06/-/blob/main/PPS_A6_Q1_pattern.c)

2) [Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_06/-/blob/main/PPS_A6_Q2_fibonacciSeq.c)
