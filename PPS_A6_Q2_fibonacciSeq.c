//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 06 - Question 02
//Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.

#include <stdio.h>

int fibonacciSeq(int x);
int fibonacciValue(int y);
int count = 0;

int main()
{
    int num;
    printf("Enter number: ");
    scanf("%d", &num);
    fibonacciSeq(num);
}

int fibonacciSeq(int x)
{
    if (count <= x)
    {
        printf("%d\n", fibonacciValue(count));
        count++;
        fibonacciSeq(x);
    }
}

int fibonacciValue(int y)
{
    if (y == 0)
    {
        return 0;
    }
    else if (y == 1)
    {
        return 1;
    }
    else
    {
        return (fibonacciValue(y - 1) + fibonacciValue(y - 2));
    }
}
