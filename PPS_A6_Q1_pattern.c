//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 06 - Question 01
//Write a recursive function pattern(n) which prints a number triangle.

#include <stdio.h>

int pattern(int n);
int rowPrint(int x);
int row = 1;

int main()
{
    int r;
    printf("Enter no of rows: ");
    scanf("%d", &r);
    pattern(r);
    return 0;
}

int pattern(int n)
{
    
    if (n > 0)
    {
        rowPrint(row);
        printf("\n");
        row++;
        pattern(n - 1);
    }
}

int rowPrint(int x)
{
    if (x > 0)
    {
        printf("%d", x);
        rowPrint(x - 1);
    }
}
